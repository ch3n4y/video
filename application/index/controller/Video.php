<?php
/**
 * Created by PhpStorm.
 * User: chen
 * Date: 18-12-25
 * Time: 下午7:56
 */

namespace app\index\controller;

use think\Db;
use think\Controller;
use think\facade\Request;


class Video extends Controller

{
    public function index()
    {
        $data = array(
            'jk'    =>  '',
            'url'   =>  ''
        );
        if($this->request->isPost())
        {
            $data['jk'] = $this->request->param('jk');
            $data['url'] = $this->request->param('url');
        }
        $url = Db::table('url')->select();
        $this->assign('url', $url);
        $this->assign('data', $data);
        return $this->fetch();
    }
}